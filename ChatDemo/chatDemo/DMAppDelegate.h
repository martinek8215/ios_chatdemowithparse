//
//  DMAppDelegate.h
//  chatDemo
//
//  Created by Adrian on 6/11/14.
//  Copyright (c) 2014 Adrian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
