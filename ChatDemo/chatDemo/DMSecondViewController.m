//
//  DMSecondViewController.m
//  chatDemo
//
//  Created by Adrian on 6/11/14.
//  Copyright (c) 2014 Adrian. All rights reserved.
//

#import "DMSecondViewController.h"

@interface DMSecondViewController ()

@end

@implementation DMSecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
